p=all cebolla
cfs=$(wildcard aoc*.cepa)
nfs=$(patsubst %.cepa,%.nim,$(cfs))
ofs=$(patsubst %.cepa,%.o,$(cfs))

%.o: %.nim
	nim c -o:$@ -r $<

%.nim: %.cepa
	cebolla

all: $(ofs)

.PHONY: $(p)
