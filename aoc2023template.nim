import strutils

proc fire() = 
  let f = open("files/01.txt")
  defer: f.close()
  var 
    p1sum = 0
    p2sum = 0
  
  
  while not f.endOfFile(): 
    let s = f.readLine()
  
  
  echo "part1:", p1sum
  echo "part2:", p2sum


fire()
