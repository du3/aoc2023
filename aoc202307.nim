import strutils, algorithm, tables, sequtils

type 
  Hand = object 
    cards*: string
    class*: int
    jclass*: int
    rank*: int
    # per convenzione sarà sempre un elemento ma full house deve gestirne due e avremo prima il valore delle tre carte e poi delle due
    powers*: seq[int]
    bid: int
  


var 
  cardsarr = "AKQJT98765432 ".reversed()
  cardsarrj = "AKQT98765432J ".reversed()



proc handsCmp(x: Hand, y: Hand): int = 
  if x.class > y.class: return -1
  elif y.class > x.class: return 1
  else: 
    var 
      xcp = 0
      ycp = 0
      ret = 0
    
    
    for i, c in x.cards: 
      let yc = y.cards[i]
      xcp = cardsarr.find(c)
      ycp = cardsarr.find(yc)
      
      if $c != $yc: break
    
    
    if xcp == ycp: ret =  0
    elif xcp > ycp: ret = -1
    else: ret = 1
    
    return ret
  


proc jhandsCmp(x: Hand, y: Hand): int = 
  if x.jclass > y.jclass: return -1
  elif y.jclass > x.jclass: return 1
  else: 
    var 
      xcp = 0
      ycp = 0
      ret = 0
    
    
    for i, c in x.cards: 
      let yc = y.cards[i]
      xcp = cardsarrj.find(c)
      ycp = cardsarrj.find(yc)
      
      if $c != $yc: break
    
    
    if xcp == ycp: ret =  0
    elif xcp > ycp: ret = -1
    else: ret = 1
    
    return ret
  


proc fire() = 
  let f = open("files/07.txt")
  defer: f.close()
  var 
    p1sum = 0
    p2sum = 0
    wintypes = newSeq[tuple[class: int, d: string]](0)
    allHands = newSeq[Hand](0)
    byClass = initTable[int, seq[int]]()
    
  
  
  wintypes.add((class:7, d: "Five of a kind")) # a
  wintypes.add((class:6, d: "Four of a kind")) # ab
  wintypes.add((class:5, d: "Full house")) #ab
  wintypes.add((class:4, d: "Three of a kind")) #abc
  wintypes.add((class:3, d: "Two pairs")) #abc
  wintypes.add((class:2, d: "One pair")) #abcd
  wintypes.add((class:1, d: "High card")) #abcde
  
  while not f.endOfFile(): 
    let s = f.readLine()
    let 
      handp = s.split(' ')
      current = allHands.len
    
    var 
      cco = initTable[char, Natural]()
      cur_hand = Hand(cards: handp[0], class:0, rank: 0, powers: @[], bid: parseInt(handp[1]))
      typs = 0
    
    for c in cur_hand.cards: 
      if cco.hasKey(c): inc cco[c]
      else: cco[c] = 1
    
    
    typs = cco.len
    
    case typs: 
      of 5: 
        cur_hand.class = 1
        cur_hand.jclass = 1
        if cco.hasKey('J'): cur_hand.jclass = 2
      
      of 4: 
        cur_hand.class = 2
        cur_hand.jclass = 2
        if cco.hasKey('J'): cur_hand.jclass = 4
      
      of 3: 
        var thr = false
        for ty, l in cco.mpairs: 
          thr = thr or (l == 3)
        
        if thr: 
          cur_hand.class = 4
          cur_hand.jclass = 4
          if cco.hasKey('J'): cur_hand.jclass = 6
        
        else: 
          cur_hand.class = 3
          cur_hand.jclass = 3
          if cco.hasKey('J'): 
            if cco['J'] == 1: cur_hand.jclass = 5
            else: cur_hand.jclass = 6
          
        
      
      of 2: 
        var thr = false
        for ty, l in cco.mpairs: 
          thr = thr or (l == 3)
        
        if thr: 
          cur_hand.class = 5
        
        else: 
          cur_hand.class = 6
        
        cur_hand.jclass = cur_hand.class
        if cco.hasKey('J'): cur_hand.jclass = 7
      
      of 1: 
        cur_hand.class = 7
        cur_hand.jclass = 7
      
      else: discard 'm'
    
    if byClass.hasKey(cur_hand.class): byClass[cur_hand.class].add(current)
    else: byClass[cur_hand.class] = @[current]
    
    allHands.add(cur_hand)
  
  
  allHands.sort(handsCmp)
  
  #for h in allHands: echo h.cards, ' ', h.bid
  let allh = allHands.len
  for i, h in allHands: 
    let mult = allh - i
    p1sum += mult * h.bid
  
  
  allHands.sort(jhandsCmp)
  
  for h in allHands: echo h.cards, ' ', h.bid
  for i, h in allHands: 
    let mult = allh - i
    p2sum += mult * h.bid
  
  
  echo "part1:", p1sum
  echo "part2:", p2sum


fire()
