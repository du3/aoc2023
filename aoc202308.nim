import strutils, tables, math, os

iterator primes(limit: int): int = 
  var isPrime = newSeq[bool](limit + 1)
  for n in 2..limit: isPrime[n] = true
  for n in 2..limit: 
    if isPrime[n]: 
      yield n
      for i in countup(n *% n, limit, n): isPrime[i] = false
    
  


proc fire() = 
  let f = open("files/08.txt")
  defer: f.close()
  var 
    p1sum = 0
    p2sum = 0
    nodes = initTable[string, tuple[l: string, r: string]]()
    line = 0
    directions: string
    point = "AAA"
    goal = "ZZZ"
    points = newSeq[string]()
    steps = newSeq[int]()
    is_goal = false
    check_goal = 0
    factors = initTable[int, int]()
  
  
  while not f.endOfFile(): 
    #stdout.write( $line )
    let s = f.readLine()
    if line == 0: directions = s
    elif line > 1: 
      #echo "s: ", s
      let 
        decl = s.split('=')
        node = decl[0].strip()
        choices = decl[1].strip(chars = {' ', '(', ')'}).split(',')
      
      
      nodes[node] = (l: choices[0], r: choices[1].strip())
      
      if node[2] == 'A': 
        points.add(node)
        steps.add(0)
      
    
    
    inc line
  
  
  block travel: 
    while point != goal: 
      for d in directions: 
        let n = nodes[point]
        if d == 'L': point = n.l
        else: point = n.r
        inc p1sum
        
        if point == goal: break travel
      
    
  
  
  #[
  block ghost_travel: 
    while not is_goal: 
      for d in directions: 
        check_goal = 0
        for id, point in points: 
          let n = nodes[point]
          if d == 'L': points[id] = n.l
          else: points[id] = n.r
          
          let controlnode = points[id]
          if controlnode[2] == 'Z': inc check_goal
        
        inc p2sum
        
        if check_goal == points.len: 
          is_goal = true
          break ghost_travel
        
      
    
  
  ]#
  
  for id, point in points: 
    var found = false
    var brk = 0
    block travel: 
      while not found: 
        for d in directions: 
          var cur = points[id]
          let n = nodes[cur]
          if d == 'L': points[id] = n.l
          else: points[id] = n.r
          inc steps[id]
          
          let np = points[id]
          # echo np
          # sleep(50)
          
          if np[2] == 'Z': 
            found = true
            echo "Got it in ", steps[id], " steps"
            break travel
          
          
          inc brk
          if brk > 5000: 
            sleep(500)
            brk = 0
            echo steps[id]
          
        
      
    
  
  
  echo steps
  
  for step in steps: 
    var lfac = initTable[int, int]()
    var exam = step
    var i = 0
    var thrsh = 100000
    var brk = 0
    while exam != 1 or i == thrsh: 
      for pri in primes(500): 
        var rem = exam %% pri
        while rem == 0: 
          if lfac.hasKey(pri): inc lfac[pri]
          else: lfac[pri] = 1
          
          exam = int(float(exam) / float(pri))
          rem = exam %% pri
        
        inc i
        inc brk
        if brk > 5000: 
          sleep(500)
          brk = 0
        
        if exam <= 1: break
      
      echo step, ' ', exam, ' ', lfac
    
    
    if exam > 1: raise newException(OSError, "Enlarge prime window (or threshold)")
    
    for prim, expo in lfac.mpairs: 
      if factors.hasKey(prim): 
        if expo > factors[prim]: factors[prim] = expo
      
      else: factors[prim] = expo
    
    
  
  
  for prim, expo in factors.mpairs: 
    echo prim, ' ', expo
    if p2sum == 0: p2sum = prim^expo
    else: p2sum *= prim^expo
  
  
  
  echo "part1:", p1sum
  echo "part2:", p2sum


fire()
