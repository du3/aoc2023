import strutils, sequtils, algorithm

type 
  St = enum 
    Generic, Getnum
  


proc getNumbers(s: string): seq[int] = 
  let digits = "0123456789"
  var vec = newSeq[int](0)
  var state = St.Generic
  var buffer = ""
  for i, c in s: 
    if digits.contains(c) or c == '-': 
      if state == St.Generic: state = St.Getnum
      buffer = buffer & c
    
    else: 
      state = St.Generic
      if buffer.len > 0: vec.add(parseInt(buffer))
      buffer = ""
    
  
  
  if state == St.Getnum: 
    if buffer.len > 0: vec.add(parseInt(buffer))
    buffer = ""
  
  
  vec


proc makeDeltas(list: seq[int]): seq[int] = 
  var vec = newSeq[int](0)
  let x = list
  #echo x
  var d = newSeq[int](0)
  for i, n in x: 
    if i > 0: 
      let prev = i - 1
      #echo n, '-', x[prev]
      var dlt: int = n - x[prev]
      #if n < 1 and dlt > 0: dlt *= -1
      d.add(dlt)
    
  
  
  vec = d
  vec



proc fire() = 
  let f = open("files/09.txt")
  defer: f.close()
  var 
    p1sum = 0
    p2sum = 0
  
  
  while not f.endOfFile(): 
    let s = f.readLine()
    echo s
    var main = s.getNumbers()
    var delta_list = newSeq[seq[int]]()
    var cnt = true
    #echo main
    while cnt: 
      if delta_list.len == 0: 
        delta_list.add(makeDeltas(main))
      
      else: 
        let last = delta_list.len - 1
        let nl = makeDeltas(delta_list[^1])
        let lg = nl.len
        let lastnl = lg - 1
        var zeros = 0
        for el in nl: 
          if el == 0: inc zeros
        
        #if nl.foldl(a + b, 0) == 0 and nl[lastnl] == 0: cnt = false
        if zeros == lg: cnt = false
        delta_list.add(nl)
      
    
    
    let dllast = delta_list.len - 1
    #echo dllast, ' ', main, ' ', delta_list
    for hist in countdown( dllast,0 ): 
      let lastid = delta_list[hist].len - 1
      let lelem = delta_list[hist][lastid]
      let llg = delta_list[hist].len
      #echo delta_list[hist]
      let checkdiv = int(delta_list[hist].foldl(a + b, 0) / llg) 
      if lelem == 0 or checkdiv == lelem: 
        delta_list[hist].add(lelem)
      
      elif hist < dllast: 
        let later = hist + 1
        let latlast = delta_list[later].len - 1
        delta_list[hist].add(lelem + delta_list[later][^1])
      
      else: 
        echo "Undefined ", delta_list, "\n----------"
      
    
    let mlast = main.len - 1
    let dlast = delta_list[0].len - 1
    let nx = main[^1] + delta_list[0][^1]
    main.add(nx)
    p1sum += nx
    echo main
    for l in delta_list: echo l
    echo "-----------"
    echo nx, ' ', p1sum
    echo "-----------"
    
    
  
  
  #[
  w: 461937579 586499582 586499622 709405480 1584748258
  ]#
  echo "part1:", p1sum
  echo "part2:", p2sum


fire()
