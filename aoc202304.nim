import strutils, parseutils, math, std/tables

type 
  St = enum 
    Generic, Getnum
  


proc getNumbers(s: string): seq[int] = 
  let digits = "0123456789"
  var vec = newSeq[int](0)
  var state = St.Generic
  var buffer = ""
  for i, c in s: 
    if digits.contains(c): 
      if state == St.Generic: state = St.Getnum
      buffer = buffer & c
    
    else: 
      state = St.Generic
      if buffer.len > 0: vec.add(parseInt(buffer))
      buffer = ""
    
  
  
  if state == St.Getnum: 
    if buffer.len > 0: vec.add(parseInt(buffer))
    buffer = ""
  
  
  vec


proc fire() = 
  let f = open("files/04.txt")
  defer: f.close()
  let digits = "0123456789"
  var 
    p1sum = 0
    p2sum = 0
    continuing = true
    copies = initTable[int, int]()
    
  
  
  while not f.endOfFile(): 
    let s = f.readLine()
    let header = s.captureBetween('C', ':').split(' ')
    let gpart = s.captureBetween(':').split('|')
    var card_id = 0
    # parse card_id
    #echo "h ", header
    for p in header: 
      if p.len > 0 and digits.contains(p[0]): 
        card_id = parseInt(p)
      
    
    let winning = getNumbers(gpart[0])
    let contenders = getNumbers(gpart[1])
    var matches = 0
    #echo "Card ", card_id, '\t', winning, contenders
    for num in contenders: 
      if num in winning: matches += 1
    
    
    var res = 2
    var ncopies = 0
    if copies.hasKey(card_id): ncopies = copies[card_id]
    
    echo "Card ", card_id, " w/ ", ncopies, " copies and ", matches, " winning numbers"
    
    if matches > 0: 
      res = res ^ (matches - 1)
      echo "(2 ^ (matches - 1)) ", res
      p1sum += res
      
      let card_after = card_id + 1
      let card_end = card_id + matches
      var adder = ncopies + 1
      
      p2sum += adder
      
      for place in card_after..card_end: 
        if not copies.hasKey(place): copies[place] = adder
        else: copies[place] += adder
      
    
    else: 
      var adder = ncopies + 1
      p2sum += adder
      continuing = false
    
    
    echo ">>>> ", p2sum
    
  
  
  echo "part1:", p1sum
  echo "part2:", p2sum


fire()
