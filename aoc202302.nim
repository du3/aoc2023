import strutils, std/tables, std/parseutils

type 
  Game = tuple[red: int, green: int, blue: int]
  GameSet = object 
    id: int
    possible: bool
    games: seq[Game]
  


proc fire() = 
  let maxes: Game = (red: 12, green:13, blue: 14)
  let f = open("files/02.txt")
  var 
    p1sum = 0
    p2sum = 0
    world = newSeq[GameSet](0)
  
  defer: f.close()
  while not f.endOfFile(): 
    var counter: Game = (red: 0, green:0, blue: 0)
    let s = f.readLine()
    var gs = GameSet(id: 0, possible: true, games: newSeq[Game](0))
    let header = s.captureBetween('G', ':').split(' ')
    var gpart = s.captureBetween(':')
    gpart.delete(0..0)
    var gseq = gpart.split("; ")
    #echo header, ':', gseq
    
    if header.len == 2: gs.id = header[1].parseInt()
    else: raise newException(OSError, "no id???")
    
    echo "Game ", gs.id, ":"
    
    for ig, gamestr in gseq: 
      var newgame: Game = (red: 0, green:0, blue: 0)
      var nouns = gamestr.split(", ")
      var se = "possible"
      for idn, noun in nouns: 
        let parts = noun.split(' ');
        let n = parseInt(parts[0])
        case parts[1]: 
          of "red": newgame.red = n
          of "blue": newgame.blue = n
          of "green": newgame.green = n
          else: echo "error parsing on ", ig
        
      
      
      if newgame.red > maxes.red or newgame.blue > maxes.blue or newgame.green > maxes.green: 
        gs.possible = false
        se = "not " & se
      
      
      echo '\t', newgame, " -- ", se
      
      gs.games.add(newgame)
      
      
    
    
    if gs.possible: p1sum += gs.id
    
    for g in gs.games: 
      if g.red > counter.red: counter.red = g.red
      if g.green > counter.green: counter.green = g.green
      if g.blue > counter.blue: counter.blue = g.blue
    
    echo "\n\tmax: ", counter
    p2sum += counter.red * counter.blue * counter.green
    #world.add(gs)
  
  
  echo "part1: ", p1sum
  echo "part2: ", p2sum


fire()
