import strutils

type 
  St = enum 
    Generic, Getnum
  
  MinMax = tuple[b: int, e: int]


proc getNumbers(s: string): seq[int] = 
  let digits = "0123456789"
  var vec = newSeq[int](0)
  var state = St.Generic
  var buffer = ""
  for i, c in s: 
    if digits.contains(c): 
      if state == St.Generic: state = St.Getnum
      buffer = buffer & c
    
    else: 
      state = St.Generic
      if buffer.len > 0: vec.add(parseInt(buffer))
      buffer = ""
    
  
  
  if state == St.Getnum: 
    if buffer.len > 0: vec.add(parseInt(buffer))
    buffer = ""
  
  
  vec


proc fire() = 
  let f = open("files/06.txt")
  defer: f.close()
  var 
    p1sum = 0
    p2sum = 0
    line = 0
    times = newSeq[int](0)
    distances = newSeq[int](0)
    stimes = newSeq[string](0)
    sdistances = newSeq[string](0)
    bigtime = 0
    bigdist = 0
  
  
  while not f.endOfFile(): 
    let s = f.readLine()
    if line == 0: times = getNumbers(s)
    elif line == 1: distances = getNumbers(s)
    inc line
  
  
  if times.len != distances.len: raise newException(OSError, "mismatching times and distances")
  
  for n in times: 
    stimes.add($n)
  
  for n in distances: 
    sdistances.add($n)
  
  bigtime = parseInt(stimes.join())
  bigdist = parseInt(sdistances.join())
  
  for i, n in times: 
    let 
      time = n
      distance = distances[i]
    
    var 
      beaten = 0
      time_pushed = 0
    
    
    echo time, ' ', distance
    for time_pushed in 1..time: 
      let time_start = time - time_pushed
      let result = time_start * time_pushed
      if result > distance: inc beaten
    
    
    if beaten == 0: echo "can't be beaten?"
    else: 
      echo "can beat ", beaten, " ways"
      if p1sum == 0: p1sum = beaten
      else: p1sum *= beaten
    
  
  
  var 
    beaten = 0
  
  
  echo bigtime, ' ', bigdist
  for time_pushed in 1..bigtime: 
    let time_start = bigtime - time_pushed
    let result = time_start * time_pushed
    if result > bigdist: inc beaten
  
  
  p2sum = beaten
  
  echo "part1:", p1sum
  echo "part2:", p2sum


fire()
