import strutils, parseutils, math, std/tables, db_connector/db_sqlite, sequtils, algorithm

type 
  St = enum 
    Generic, Getnum
  
  Seed = tuple[seed: int, soil: int, fert: int, water: int, light: int, temp: int, humi: int, loc: int]
  MinMax = tuple[b: int, e: int]
  MapSt = enum 
    Free = 0, Soil, Fert, Water, Light, Temp, Humi, Loc
  
  SRKeys = enum 
    minSeed, maxSeed, minSoil, maxSoil, minFert, maxFert, minWater, maxWater, minLight, maxLight, minTemp, maxTemp, minHumi, maxHumi, minLoc, maxLoc
  
  SeedRange = table[Natural, int]


proc getNumbers(s: string): seq[int] = 
  let digits = "0123456789"
  var vec = newSeq[int](0)
  var state = St.Generic
  var buffer = ""
  for i, c in s: 
    if digits.contains(c): 
      if state == St.Generic: state = St.Getnum
      buffer = buffer & c
    
    else: 
      state = St.Generic
      if buffer.len > 0: vec.add(parseInt(buffer))
      buffer = ""
    
  
  
  if state == St.Getnum: 
    if buffer.len > 0: vec.add(parseInt(buffer))
    buffer = ""
  
  
  vec


proc cloneIfBlank(stat: MapSt, seed: var Seed) = 
  case stat: 
    of MapSt.Soil: 
      if seed.soil == -1: seed.soil = seed.seed
    
    of MapSt.Fert: 
      if seed.fert == -1: seed.fert = seed.soil
    
    of MapSt.Water: 
      if seed.water == -1: seed.water = seed.fert
    
    of MapSt.Light: 
      if seed.light == -1: seed.light = seed.water
    
    of MapSt.Temp: 
      if seed.temp == -1: seed.temp = seed.light
    
    of MapSt.Humi: 
      if seed.humi == -1: seed.humi = seed.temp
    
    of MapSt.Loc: 
      if seed.loc == -1: seed.loc = seed.humi
    
    else: 
      discard "oooooo"
    
  


proc fieldMutate(fsource: var int, fdest: var int, so_start: int, so_end: int, de_start: int): bool = 
  if fdest == -1: 
    if fsource >= so_start and fsource <= so_end: 
      let d = fsource - so_start
      fdest = de_start + d
      return true
    
  
  
  false


proc rep(seed: Seed) = 
  echo "MATCH! ", seed


proc passLine(stat: MapSt, seed: var Seed, so_start: int, so_end: int, de_start: int) = 
  case stat: 
    of MapSt.Soil: 
      if fieldMutate(seed.seed, seed.soil, so_start, so_end, de_start): rep(seed)
    
    of MapSt.Fert: 
      if fieldMutate(seed.soil, seed.fert, so_start, so_end, de_start): rep(seed)
    
    of MapSt.Water: 
      if fieldMutate(seed.fert, seed.water, so_start, so_end, de_start): rep(seed)
    
    of MapSt.Light: 
      if fieldMutate(seed.water, seed.light, so_start, so_end, de_start): rep(seed)
    
    of MapSt.Temp: 
      if fieldMutate(seed.light, seed.temp, so_start, so_end, de_start): rep(seed)
    
    of MapSt.Humi: 
      if fieldMutate(seed.temp, seed.humi, so_start, so_end, de_start): rep(seed)
    
    of MapSt.Loc: 
      if fieldMutate(seed.humi, seed.loc, so_start, so_end, de_start): rep(seed)
    
    else: 
      discard "oooooo"
    
  


proc fire() = 
  let f = open("files/05.txt")
  let db = db_sqlite.open("seed.db", "", "", "");
  defer: 
    f.close()
    db.close()
  
  var 
    p1sum = 0
    p2sum = 0
    line = 0
    seeds = newSeq[Seed](0)
    keymap = { "soil": MapSt.Soil, "fertilizer": Fert, "water": Water, "light": Light, "temperature": Temp, "humidity": Humi, "location": Loc }.toTable
    scan_state = MapSt.Free
    seed_range_template = initTable[Natural, int]
    seed_ranges = newSeq[SeedRange](0)
  
  let 
    seed_template: Seed = (seed: -1, soil: -1, fert: -1, water: -1, light: -1, temp: -1, humi: -1, loc: -1)
    seed_fields = "seed soil fertilizer water light temperature humidity location".split(' ')
  
  
  var header = newSeq[string](0)
  for f in seed_fields: header.add( "min_" & f & " BIGINT NULL,\nmax_" & f & " BIGINT NULL" )
  db.exec(sql"DROP TABLE IF EXISTS seeds;");
  echo " CREATE TABLE seeds ( " & header.join(",\n")  & "); "
  db.exec(sql(" CREATE TABLE seeds ( " & header.join(",\n")  & "); "))
  header = newSeq[string](0)
  var record = newSeq[string](0)
  for f in seed_fields: 
    header.add("min_" & f)
    header.add("max_" & f)
    record.add("0")
    record.add("999999999999")
  
  db.exec(sql( " INSERT INTO seeds (" & header.join(", ") & ") values (" & record.join(", ") &  "); " ))
  
  while not f.endOfFile(): 
    let s = f.readLine()
    echo "scan ", s
    var 
      newid = seeds.len
      source = -1
      dest = -1
    
    
    if line == 0: 
      var bufseeds = getNumbers(s)
      var buffer = newSeq[int](0)
      for b in bufseeds: 
        seeds.add(seed_template)
        seeds[newid].seed = b
        newid = seeds.len
        buffer.add(b)
        if buffer.len > 1: 
          let p_end = buffer[0] + ( buffer[1] - 1 )
          seed_pattern_space.add((buffer[0], p_end))
          buffer = newSeq[int](0)
        
      
    
    else: 
      if s.len == 0: 
        if scan_state != MapSt.Free: 
          # mappa i seeds con campo non trovato al campo precedente
          for i, seed in seeds: 
            cloneIfBlank(scan_state, seeds[i])
          
        
        
        echo seeds
        scan_state = MapSt.Free
        echo "blank."
      
      else: 
        if s.contains("map"): 
          let wsparts = s.split(' ')
          let dparts = wsparts[0].split('-')
          let dest = dparts[2]
          scan_state = keymap[dest]
          echo dparts
          echo "state ",scan_state
        
        elif scan_state != MapSt.Free: 
          var 
            patternspace = getNumbers(s)
          
          let 
            so_start = patternspace[1]
            de_start = patternspace[0]
            rang = patternspace[2]
          
          let endpos = rang - 1
          let 
            so_end = so_start + endpos
            de_end = de_start + endpos
          
          echo "from ", so_start, " to ", so_end, " with ", de_start, " range ", endpos
          
          for i, seed in seeds: 
            passLine(scan_state, seeds[i], so_start, so_end, de_start)
          
        
      
    
    line += 1
  
  echo seed_pattern_space
  
  p1sum = high(int)
  
  echo '-'
  echo seeds
  for i, seed in seeds: 
    if seeds[i].loc == -1: seeds[i].loc = seeds[i].humi
    if p1sum > seeds[i].loc: p1sum = seeds[i].loc
  
  
  #[
  wrongAnswers: 496859573
  rightAnswer: 57075758
  ]#
  echo "part1:", p1sum
  #[
  
  ]#
  echo "part2:", p2sum


fire()
