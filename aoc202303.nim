import strutils, std/tables

type 
  St = enum 
    Generic, Getnum
  
  Point = tuple[x: int, y: int]
  PotentialNumber = tuple[num: int, startX: int, endX:int, Y:int]



proc fire() = 
  let f = open("files/03.txt")
  let digits = "0123456789"
  # grepped the symbols from file :)
  let symbols = "#$%&*+-/=@"
  let pn: PotentialNumber = (num: 0, startX: 0, endX: 0, Y: 0)
  defer: f.close()
  var 
    p1sum = 0
    p2sum = 0
    line = 0
    symbol_found = initTable[string, char]()
    gears_found = initTable[string, seq[int]]()
    number_found = newSeq[PotentialNumber](0)
  
  
  while not f.endOfFile(): 
    let s = f.readLine()
    var 
      parsed_number = 0
      number_extracted = ""
      scan_state = St.Generic
      current_num = 0
      scan_report = "line: " & $line & "<\t"
    
    
    number_found.add(pn)
    current_num = number_found.len - 1
    number_found[current_num].Y = line
    #echo "line: ", line, ")\t", s
    for i, c in s: 
      if digits.contains(c): 
        if scan_state == St.Generic: 
          scan_state = St.Getnum
          number_found[current_num].startX = i
          scan_report = scan_report & '#'
        
        else: 
          scan_report = scan_report & ' '
        
        
        number_extracted = number_extracted & c
      
      else: 
        scan_state = St.Generic
        
        if number_extracted.len > 0: 
          number_found[current_num].endX = i - 1
          number_found[current_num].num = parseInt(number_extracted)
        
        
        if number_found[current_num].num == 0 and number_found[current_num].startX == 0 and number_found[current_num].endX == 0: number_found.delete(current_num)
        number_found.add(pn)
        current_num = number_found.len - 1
        number_found[current_num].Y = line
        
        parsed_number = 0
        number_extracted = ""
        
        if symbols.contains(c): 
          let k = $i & '-' & $line
          symbol_found[k] = c
          scan_report = scan_report & '^'
        
        else: scan_report = scan_report & ' '
        
      
    
    # gestire numero riportato a fine riga
    if scan_state == St.Getnum and number_extracted.len > 0: 
      number_found[current_num].endX = s.len - 1
      number_found[current_num].num = parseInt(number_extracted)
    
    #echo scan_report
    
    line += 1
    if number_found[current_num].num == 0 and number_found[current_num].startX == 0 and number_found[current_num].endX == 0: number_found.delete(current_num)
  
  
  #echo symbol_found
  for nf in number_found: 
    var 
      find_square = newSeq[string](0)
      upperBound = nf.Y - 1
      lowerBound = nf.Y + 1
      left = nf.startX - 1
      right = nf.endX + 1
      found = false
    
    
    for x in left..right: 
      for y in upperBound..lowerBound: 
        find_square.add($x & '-' & $y)
      
    
    
    #echo nf
    #echo find_square
    for k in find_square: 
      if symbol_found.hasKey(k): 
        #echo nf.num, " found"
        found = true
        if symbol_found[k] == '*': 
          if not gears_found.hasKey(k): 
            gears_found[k] = @[nf.num]
          
          else: 
            gears_found[k].add(nf.num)
          
        
      
    
    
    if found: p1sum += nf.num
    
  
  #echo gears_found
  for k, sq in gears_found: 
    if sq.len == 2: p2sum += sq[0] * sq[1]
  
  
  echo "part1:", p1sum
  echo "part2:", p2sum


fire()
